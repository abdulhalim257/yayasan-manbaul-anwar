<?php
/**
 * Displays footer site info
 *
 * @package Inspiro
 * @subpackage Inspiro_Lite
 * @since Inspiro 1.0.0
 * @version 1.0.0
 */

?>
<div class="site-info">
	<?php
	$d=date('Y');
	if ( function_exists( 'the_privacy_policy_link' ) ) {
		the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
	}
	?>
		
	<span class="copyright">
		<span>
			<a href="<?php echo esc_url( __( 'https://www.MIB.com/', 'inspiro' ) ); ?>" target="_blank">
				<?php
				/* translators: %s: WordPress trademark */
				printf( esc_html__( 'Powered by %s' ), 'Media Inovasi Berkarya') ;
				?>
			</a>
		</span>
		<span>
			<?php esc_html_e( '', 'inspiro' ); ?> <a href="<?php echo 'https://www.MIB.com/'; ?>" target="_blank" rel="nofollow"><?php echo"MIB &copy; ".$d;?></a>
		</span>
	</span>
</div><!-- .site-info -->
